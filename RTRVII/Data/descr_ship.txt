;	
;	This file contains the mount linkages; it gets parsed on application 
;	startup but is not otherwise referred to. The data format is thus:
;
;	;						indicates a comment ;-)
;							
;	type					indicates a new ship type, must be followed by id name string
;	ship_type				either flag, transport, war
;	size					size of the ship in metres (10-50)
;	speed					speed of the ship in kmph (1-20)
;	power					method of power	(sail, oars)
;	artillery				artillery type (flame, rock) - this should refer to the engine_db eventually
;	ram						(yes, no)
;	durability				damage ship can take before sinking (0-20)
;	armour					armour value of ship (0-20)
;	depth					how much water it draws (in m), depth of water needed to operate
;	beam					height of the sides, (for boarding, firing archers ect)


type						heavy warship
ship_type					war
carrying_capacity			0
size						35
speed						10
power						sail
artillery					rock
ram							yes
durability					10
armour						10
depth						15
beam						15

type						light warship
ship_type					war
carrying_capacity			0
size						20
speed						20
power						oars
artillery					flame
ram							no
durability					5
armour						5
depth						10
beam						10

type						flagship
ship_type					flag
carrying_capacity			0
size						50
speed						15
power						sail
artillery					rock
ram							yes
durability					15
armour						15
depth						20
beam						20
