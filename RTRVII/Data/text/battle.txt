�   S t r i n g s   t h a t   o n l y   a p p e a r   i n   b a t t l e  
 { B M T _ T U R T L E } 	 	 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   T e s t u d o   f o r m a t i o n  
 { B M T _ P H A L A N X } 	 	 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   P h a l a n x   f o r m a t i o n  
 { B M T _ T O G G L E _ S K I R M I S H _ O N } 	 	 	 	 	 	 	 	 E n a b l e   s k i r m i s h   m o d e  
 { B M T _ T O G G L E _ F I R E _ O N } 	 	 	 	 	 	 	 	 	 E n a b l e   f i r e   a t   w i l l   m o d e  
 { B M T _ T O G G L E _ S K I R M I S H _ O F F } 	 	 	 	 	 	 	 	 D i s a b l e   s k i r m i s h   m o d e  
 { B M T _ T O G G L E _ F I R E _ O F F } 	 	 	 	 	 	 	 	 	 D i s a b l e   f i r e   a t   w i l l   m o d e  
 { B M T _ T O G G L E _ D E F E N D _ O N } 	 	 	 	 	 	 	 	 E n a b l e   g u a r d   m o d e    
 { B M T _ T O G G L E _ D E F E N D _ O F F } 	 	 	 	 	 	 	 	 D i s a b l e   g u a r d   m o d e    
 { B M T _ T O G G L E _ F O R M A T I O N _ T I G H T } 	 	 	 	 	 	 	 S e t   u n i t   f o r m a t i o n   t o   ' t i g h t '  
 { B M T _ T O G G L E _ F O R M A T I O N _ L O O S E } 	 	 	 	 	 	 	 S e t   u n i t   f o r m a t i o n   t o   ' l o o s e '  
 { B M T _ T O G G L E _ S P E C I A L _ F O R M A T I O N _ O N } 	 	 	 	 	 	 S e t   u n i t s   t o   s p e c i a l i s t   f o r m a t i o n   ( t e s t u d o / p h a l a n x   e t c . )  
 { B M T _ T O G G L E _ S P E C I A L _ F O R M A T I O N _ O F F } 	 	 	 	 	 S e t   u n i t s   t o   n o r m a l   f o r m a t i o n  
 { B M T _ R A D A R _ Z O O M _ I N } 	 	 	 	 	 	 	 	 	 Z o o m   i n   o n   c u r r e n t   l o c a t i o n  
 { B M T _ R A D A R _ Z O O M _ O U T } 	 	 	 	 	 	 	 	 	 Z o o m   o u t   o f   c u r r e n t   l o c a t i o n  
 { B M T _ S H O W _ G R O U P _ F O R M A T I O N S } 	 	 	 	 	 	 	 S h o w   g r o u p   f o r m a t i o n   b u t t o n s  
 { B M T _ H I D E _ G R O U P _ F O R M A T I O N S } 	 	 	 	 	 	 	 H i d e   g r o u p   f o r m a t i o n   b u t t o n s  
 { B M T _ H A L T } 	 	 	 	 	 	 	 	 	 	 	 S t o p   c u r r e n t   o r d e r s  
 { B M T _ W A L K } 	 	 	 	 	 	 	 	 	 	 	 W a l k  
 { B M T _ R U N } 	 	 	 	 	 	 	 	 	 	 	 	 R u n  
 { B M T _ W I T H D R A W } 	 	 	 	 	 	 	 	 	 	 W i t h d r a w  
 { B M T _ P A U S E } 	 	 	 	 	 	 	 	 	 	 	 P a u s e   g a m e  
 { B M T _ N E W _ P L A Y } 	 	 	 	 	 	 	 	 	 	 U n p a u s e   g a m e   a n d   s e t   t o   s i n g l e   s p e e d  
 { B M T _ D O U B L E } 	 	 	 	 	 	 	 	 	 	 	 D o u b l e   g a m e   s p e e d  
 { B M T _ T R I P L E } 	 	 	 	 	 	 	 	 	 	 	 T r i p l e   g a m e   s p e e d  
 { B M T _ R A D A R } 	 	 	 	 	 	 	 	 	 	 	 R a d a r  
 { B M T _ G R O U P } 	 	 	 	 	 	 	 	 	 	 	 G r o u p   s e l e c t e d   u n i t s  
 { B M T _ U N G R O U P } 	 	 	 	 	 	 	 	 	 	 	 U n g r o u p   s e l e c t e d   u n i t s  
 { B M T _ F A T _ F R E S H } 	 	 	 	 	 	 	 	 	 	 F r e s h  
 { B M T _ F A T _ W A R M E D _ U P } 	 	 	 	 	 	 	 	 	 W a r m e d   u p  
 { B M T _ F A T _ W I N D E D } 	 	 	 	 	 	 	 	 	 	 W i n d e d  
 { B M T _ F A T _ T I R E D } 	 	 	 	 	 	 	 	 	 	 T i r e d  
 { B M T _ F A T _ M O S T _ T I R E D } 	 	 	 	 	 	 	 	 	 E x h a u s t e d  
 { B M T _ F A T _ E X H A U S T E D } 	 	 	 	 	 	 	 	 	 V e r y   t i r e d  
 { B M T _ M O R _ B E S E R K } 	 	 	 	 	 	 	 	 	 	 B e r s e r k  
 { B M T _ M O R _ I M P E T U O U S } 	 	 	 	 	 	 	 	 	 I m p e t u o u s  
 { B M T _ M O R _ H I G H } 	 	 	 	 	 	 	 	 	 	 E a g e r  
 { B M T _ M O R _ F I R M } 	 	 	 	 	 	 	 	 	 	 S t e a d y  
 { B M T _ M O R _ S H A K E N } 	 	 	 	 	 	 	 	 	 	 S h a k e n  
 { B M T _ M O R _ W A V E R I N G } 	 	 	 	 	 	 	 	 	 W a v e r i n g  
 { B M T _ M O R _ R O U T I N G } 	 	 	 	 	 	 	 	 	 	 B r o k e n  
 { B M T _ E N D _ D E P L O Y M E N T } 	 	 	 	 	 	 	 	 	 S T A R T   B A T T L E  
 { B M T _ W A I T } 	 	 	 	 	 	 	 	 	 	 	 W A I T  
 { B M T _ S T A R T _ D E P L O Y M E N T } 	 	 	 	 	 	 	 	 	 S T A R T   D E P L O Y M E N T  
 { B M T _ C H A T _ S U P P O R T } 	 	 	 	 	 	 	 	 	 S u p p o r t  
 { B M T _ C H A T _ R E T R E A T } 	 	 	 	 	 	 	 	 	 R e t r e a t  
 { B M T _ C H A T _ A D V A N C E } 	 	 	 	 	 	 	 	 	 A d v a n c e  
 { B M T _ C H A T _ A T T A C K } 	 	 	 	 	 	 	 	 	 	 A t t a c k  
 { B M T _ C H A T _ H E L P } 	 	 	 	 	 	 	 	 	 	 O f f e r   h e l p  
 { B M T _ C H A T _ S U P P O R T _ F L A N K } 	 	 	 	 	 	 	 	 S u p p o r t   m y   f l a n k  
 { B M T _ C H A T _ S U P P O R T _ A T T A C K } 	 	 	 	 	 	 	 	 S u p p o r t   m y   n e x t   a t t a c k  
 { B M T _ C H A T _ S U P P O R T _ A D V A N C E } 	 	 	 	 	 	 	 	 S u p p o r t   m y   a d v a n c e  
 { B M T _ C H A T _ R E T R E A T _ F L A N K } 	 	 	 	 	 	 	 	 P u l l   b a c k   t o   c o v e r   m y   f l a n k  
 { B M T _ C H A T _ R E T R E A T _ G E N E R A L } 	 	 	 	 	 	 	 	 G e t   t h e   h e l l   o u t   o f   h e r e  
 { B M T _ C H A T _ A D V A N C E _ L I N E } 	 	 	 	 	 	 	 	 	 A d v a n c e   a l o n g   b a t t l e   l i n e  
 { B M T _ C H A T _ A D V A N C E _ K E E P _ U P } 	 	 	 	 	 	 	 	 K e e p   u p   w i t h   m y   t r o o p s  
 { B M T _ C H A T _ A T T A C K _ A L L } 	 	 	 	 	 	 	 	 	 A t t a c k   a n y o n e  
 { B M T _ C H A T _ R E T R E A T _ T O _ P O S } 	 	 	 	 	 	 	 	 F a l l   b a c k   t o   t h e   p o s i t i o n   s h o w n  
 { B M T _ C H A T _ A D V A N C E _ T O _ P O S } 	 	 	 	 	 	 	 	 A d v a n c e   t o   t h e   p o s i t i o n   s h o w n  
 { B M T _ C H A T _ A T T A C K _ P O S } 	 	 	 	 	 	 	 	 	 A t t a c k   u n i t s   a t   t h e   p o s i t i o n   s h o w n  
 { B M T _ C H A T _ U S E R } 	 	 	 	 	 	 	 	 	 	 C u s t o m  
 { B M T _ C H A T _ C A N C E L } 	 	 	 	 	 	 	 	 	 	 C a n c e l  
 { B M T _ C H A T _ H E L P _ B O D Y } 	 	 	 	 	 	 	 	 	 I ' m   h e r e   t o   h e l p .     T e l l   m e   w h a t   t o   d o .  
 { B M T _ S P E C I F Y _ P O S I T I O N } 	 	 	 	 	 	 	 	 	 P o i n t   a t   a n   a r e a   o f   t h e   m a p   a n d   s p e c i f y   a   p o s i t i o n   w i t h   t h e   ' o r d e r '   m o u s e   b u t t o n  
 { B M T _ S I E G E _ E Q U I P M E N T } 	 	 	 	 	 	 	 	 	 S i e g e   e q u i p m e n t  
 { B M T _ D E P L O Y _ S I E G E } 	 	 	 	 	 	 	 	 	 D r a g   e q u i p m e n t   i c o n   o n t o   a   u n i t   c a r d   t o   d e p l o y  
 { B M T _ A C T _ I D L E } 	 	 	 	 	 	 	 	 	 	 I d l e  
 { B M T _ A C T _ H I D I N G } 	 	 	 	 	 	 	 	 	 	 H i d i n g  
 { B M T _ A C T _ R E A D Y } 	 	 	 	 	 	 	 	 	 	 R e a d y  
 { B M T _ A C T _ R E F O R M I N G } 	 	 	 	 	 	 	 	 	 R e f o r m i n g  
 { B M T _ A C T _ M O V I N G } 	 	 	 	 	 	 	 	 	 	 M a r c h i n g  
 { B M T _ A C T _ F I R I N G } 	 	 	 	 	 	 	 	 	 	 F i r i n g   m i s s i l e s  
 { B M T _ A C T _ R E L O A D I N G } 	 	 	 	 	 	 	 	 	 R e l o a d i n g  
 { B M T _ A C T _ C H A R G I N G } 	 	 	 	 	 	 	 	 	 C h a r g i n g  
 { B M T _ A C T _ F I G H T I N G } 	 	 	 	 	 	 	 	 	 F i g h t i n g  
 { B M T _ A C T _ R U N N I N G _ A M O K } 	 	 	 	 	 	 	 	 R u n n i n g   a m o k  
 { B M T _ A C T _ R O U T I N G } 	 	 	 	 	 	 	 	 	 	 R o u t i n g  
 { B M T _ A C T _ F I G H T I N G _ B A C K S _ T O _ T H E _ W A L L S } 	 	 	 	 	 F i g h t i n g   t o   t h e   d e a t h  
 { B M T _ A C T _ P U R S U I N G } 	 	 	 	 	 	 	 	 	 P u r s u i n g  
 { B M T _ A C T _ D E A D } 	 	 	 	 	 	 	 	 	 	 D e a d  
 { B M T _ A C T _ L E F T } 	 	 	 	 	 	 	 	 	 	 L e f t   b a t t l e  
 { B M T _ A C T _ E N T E R I N G _ B A T T L E } 	 	 	 	 	 	 	 	 W a i t i n g   t o   e n t e r   b a t t l e  
 { B M T _ A C T _ T A U N T I N G } 	 	 	 	 	 	 	 	 	 T a u n t i n g  
 { B M T _ C O M _ W I N _ B I G } 	 	 	 	 	 	 	 	 	 	 W i n n i n g   e a s i l y  
 { B M T _ C O M _ W I N } 	 	 	 	 	 	 	 	 	 	 	 W i n n i n g  
 { B M T _ C O M _ W I N _ S L I G H T } 	 	 	 	 	 	 	 	 	 W i n n i n g   s l i g h t l y  
 { B M T _ C O M _ E V E N } 	 	 	 	 	 	 	 	 	 	 E v e n l y   m a t c h e d  
 { B M T _ C O M _ L O S E _ S L I G H T } 	 	 	 	 	 	 	 	 	 L o s i n g   s l i g h t l y  
 { B M T _ C O M _ L O S E } 	 	 	 	 	 	 	 	 	 	 L o s i n g  
 { B M T _ C O M _ L O S E _ B I G } 	 	 	 	 	 	 	 	 	 L o s i n g   h e a v i l y  
 { B M T _ D A M A G E } 	 	 	 	 	 	 	 	 	 	 	 D a m a g e :  
 { B M T _ H O L D _ F O R _ X _ M I N U T E S } 	 	 	 	 	 	 	 	 L o c a t i o n   m u s t   b e   h e l d   f o r   % f   m i n u t e s .  
 { B M T _ C A P T U R E _ L O C A T I O N } 	 	 	 	 	 	 	 	 	 C a p t u r e   l o c a t i o n  
 { B M T _ C A P T U R E _ S E T T L E M E N T } 	 	 	 	 	 	 	 	 C a p t u r e   s e t t l e m e n t  
 { B M T _ C A P T U R E _ C A M P } 	 	 	 	 	 	 	 	 	 C a p t u r e   b a g g a g e   t r a i n  
 { B M T _ D E S T R O Y _ C H A R A C T E R } 	 	 	 	 	 	 	 	 	 D e s t r o y   c h a r a c t e r  
 { B M T _ D E S T R O Y _ E N E M Y } 	 	 	 	 	 	 	 	 	 D e s t r o y   o r   r o u t   e n e m y  
 { B M T _ B A L A N C E _ O F _ S T R E N G T H } 	 	 	 	 	 	 	 	 B a l a n c e   o f   p o w e r   ( p e r c e n t )  
 { B M T _ D E S T R O Y _ E N E M Y _ S T R E N G T H } 	 	 	 	 	 	 	 	 R e d u c e   e n e m y   s t r e n g t h  
 { B M T _ S U C C E E D E D } 	 	 	 	 	 	 	 	 	 	 S u c c e e d e d  
 { B M T _ F A I L E D } 	 	 	 	 	 	 	 	 	 	 	 F a i l e d  
 { B M T _ I M P O S S I B L E } 	 	 	 	 	 	 	 	 	 	 I m p o s s i b l e  
 { B M T _ C O N D I T I O N S } 	 	 	 	 	 	 	 	 	 	 B a t t l e   o b j e c t i v e s  
 { B M T _ C O N D I T I O N S _ A L L } 	 	 	 	 	 	 	 	 	 B a t t l e   o b j e c t i v e s   ( a l l   r e q u i r e d )  
 { B M T _ C O N D I T I O N S _ A N Y } 	 	 	 	 	 	 	 	 	 B a t t l e   o b j e c t i v e s   ( o n e   o r   m o r e   r e q u i r e d )  
 { B M T _ S T A T E _ R U N N I N G } 	 	 	 	 	 	 	 	 	 R u n n i n g  
 { B M T _ S T A T E _ W A L K I N G } 	 	 	 	 	 	 	 	 	 W a l k i n g  
 { B M T _ S T A T E _ U N D E R _ M I S S I L E _ A T T A C K } 	 	 	 	 	 	 U n d e r   m i s s i l e   a t t a c k  
 { B M T _ S T A T E _ P A N I C } 	 	 	 	 	 	 	 	 	 	 P a n i c k i n g  
 { B M T _ S T A T E _ H I D I N G } 	 	 	 	 	 	 	 	 	 H i d i n g  
 { B M T _ S T A T E _ A T T A C K I N G } 	 	 	 	 	 	 	 	 	 A t t a c k i n g  
 { B M T _ S T A T E _ A T T A C K _ M I S S I L E } 	 	 	 	 	 	 	 A t t a c k i n g   w i t h   m i s s i l e s  
 { B M T _ S T A T E _ L A D D E R S } 	 	 	 	 	 	 	 	 	 C a r r y i n g   l a d d e r s  
 { B M T _ S T A T E _ R A M S } 	 	 	 	 	 	 	 	 	 	 C a r r y i n g   b a t t e r i n g   r a m  
 { B M T _ S T A T E _ S A P P E R S } 	 	 	 	 	 	 	 	 	 S a p p e r s  
 { B M T _ S T A T E _ T O W E R S } 	 	 	 	 	 	 	 	 	 A t t a c h e d   t o   s i e g e   t o w e r  
 { B M T _ E N C O U R A G E D _ A R M Y _ S U P P O R T E D } 	 	 	 	 	 	 E n c o u r a g e d   b y   n e a r b y   a r m i e s   i n   t h e   c a m p a i g n  
 { B M T _ E N C O U R A G E D _ A R M Y _ N O _ R E T R E A T } 	 	 	 	 	 	 G r i m l y   d e t e r m i n e d   s i n c e   t h e r e   i s   n o   r e t r e a t   i n   t h e   c a m p a i g n  
 { B M T _ E N C O U R A G E D _ G E N E R A L _ P R E S E N T } 	 	 	 	 	 	 G l a d   t o   h a v e   t h e   g e n e r a l   i n   t h e   u n i t  
 { B M T _ E N C O U R A G E D _ G E N E R A L _ N E A R B Y } 	 	 	 	 	 	 S p i r i t s   l i f t e d   b y   t h e   g e n e r a l ' s   e n c o u r a g e m e n t s  
 { B M T _ E N C O U R A G E D _ C O M M A N D _ U N I T _ N E A R B Y } 	 	 	 	 	 H a p p y   t o   b e   n e a r   a   c o m m a n d   u n i t  
 { B M T _ E N C O U R A G E D _ F L A N K S _ S E C U R E } 	 	 	 	 	 	 S a f e   i n   t h e   k n o w l e d g e   t h a t   t h e   f l a n k s   a r e   s e c u r e  
 { B M T _ E N C O U R A G E D _ F O R T I F I C A T I O N } 	 	 	 	 	 	 P l e a s e d   t o   b e   c l o s e   t o   a   f r i e n d l y   f o r t i f i c a t o n  
 { B M T _ E N C O U R A G E D _ O N _ T H E _ H I L L } 	 	 	 	 	 	 	 F e e l i n g   s e c u r e   o n   t h e   h i l l  
 { B M T _ E N C O U R A G E D _ E N E M Y _ R O U T I N G } 	 	 	 	 	 	 G l a d   t o   s e e   t h e   e n e m y   r o u t i n g  
 { B M T _ E N C O U R A G E D _ T R Y I N G _ T O _ R A L L Y } 	 	 	 	 	 	 T r y i n g   t o   r a l l y  
 { B M T _ E N C O U R A G E D _ H I D I N G } 	 	 	 	 	 	 	 	 H a p p y   t o   b e   h i d d e n  
 { B M T _ E N C O U R A G E D _ B L O O D L U S T } 	 	 	 	 	 	 	 I n   b l o o d l u s t   a n d   o b l i v i o u s   t o   t h e   w o r l d  
 { B M T _ E N C O U R A G E D _ B E R S E R K } 	 	 	 	 	 	 	 	 I n   b e r s e r k   b l o o d l u s t   a n d   o b l i v i o u s   t o   t h e   w o r l d  
 { B M T _ C O N C E R N E D _ A R M Y _ D E S T R U C T I O N } 	 	 	 	 	 	 D i s m a y e d   a t   t h e   l o s s   o f   t h e   b a t t l e  
 { B M T _ C O N C E R N E D _ G E N E R A L _ J U S T _ D I E D } 	 	 	 	 	 	 P a n i c k e d   o v e r   t h e   g e n e r a l ' s   d e a t h  
 { B M T _ C O N C E R N E D _ F L A N K S _ E X P O S E D } 	 	 	 	 	 	 C o n c e r n e d   o v e r   e x p o s e d   f l a n k s  
 { B M T _ C O N C E R N E D _ F A T I G U E } 	 	 	 	 	 	 	 	 U n h a p p y   d u e   t o   e x h a u s t i o n  
 { B M T _ C O N C E R N E D _ C A S U A L T I E S } 	 	 	 	 	 	 	 U n h a p p y   o v e r   t a k i n g   c a s u a l t i e s  
 { B M T _ C O N C E R N E D _ A T T A C K E D _ B Y _ F I R E } 	 	 	 	 	 	 P a n i c k e d   b y   f i r e   a t t a c k  
 { B M T _ C O N C E R N E D _ A T T A C K E D _ B Y _ A R T I L L E R Y } 	 	 	 	 	 P a n i c k e d   b y   a r t i l l e r y   f i r e  
 { B M T _ C O N C E R N E D _ F I G H T I N G _ C A V A L R Y } 	 	 	 	 	 	 C o n c e r n e d   a b o u t   f i g h t i n g   a   c a v a l r y   u n i t  
 { B M T _ C O N C E R N E D _ F R I E N D S _ R O U T I N G } 	 	 	 	 	 	 U n h a p p y   t o   s e e   f r i e n d s   r o u t i n g  
 { B M T _ C O N C E R N E D _ E N E M Y _ N U M B E R S } 	 	 	 	 	 	 	 D i s t r a u g h t   o v e r   t h e   n u m b e r   o f   e n e m i e s  
 { B M T _ C O N C E R N E D _ S U R P R I S E D } 	 	 	 	 	 	 	 	 S u r p r i s e d   b y   t h e   e n e m y   e m e r g i n g  
 { B M T _ C O N C E R N E D _ P A N I C } 	 	 	 	 	 	 	 	 	 F l e e i n g   i n   p a n i c   a n d   o b l i v i o u s   t o   t h e   w o r l d  
 { B M T _ C O N C E R N E D _ F E A R _ P I G S } 	 	 	 	 	 	 	 	 F r i g h t e n e d   b y   p i g s  
 { B M T _ C O N C E R N E D _ F E A R _ D O G S } 	 	 	 	 	 	 	 	 F r i g h t e n e d   b y   d o g s  
 { B M T _ C O N C E R N E D _ F E A R _ E L E P H A N T S } 	 	 	 	 	 	 F r i g h t e n e d   b y   e l e p h a n t s  
 { B M T _ C O N C E R N E D _ F E A R _ C H A R I O T S } 	 	 	 	 	 	 	 F r i g h t e n e d   b y   c h a r i o t s  
 { B M T _ C O N C E R N E D _ F E A R _ E N E M Y _ U N I T } 	 	 	 	 	 	 I n t i m i d a t e d   b y   n e a r b y   e n e m y  
 { B M T _ D E S T R O Y _ O R _ R O U T _ E N E M Y } 	 	 	 	 	 	 	 D e s t r o y   o r   r o u t   a l l   e n e m i e s  
 { B M T _ A C H I E V E _ B A L A N C E _ O F _ S T R E N G T H _ O F _ X _ P E R C E N T } 	 	 A c h i e v e   b a l a n c e   o f   s t r e n g t h   o f   % d % %  
 { B M T _ D E S T R O Y _ E N E M Y _ S T R E N G T H _ X _ P E R C E N T } 	 	 	 	 D e s t r o y   % d % %   o f   e n e m y   s t r e n g t h  
 { B M T _ W I T H _ M I N _ X _ A L L I E S } 	 	 	 	 	 	 	 	 W i t h   a t   l e a s t   % d   a l l i e d   s o l d i e r s  
 { B M T _ W I T H _ M A X _ X _ E N E M Y } 	 	 	 	 	 	 	 	 A n d   a t   m o s t   % d   e n e m y   s o l d i e r s  
 { B M T _ L O C A T I O N } 	 	 	 	 	 	 	 	 	 	 L o c a t i o n  
 { B M T _ D R O P _ S I E G E _ E Q U I P M E N T } 	 	 	 	 	 	 	 D r o p   s i e g e   e q u i p m e n t  
 { B M T _ A U T O D E P L O Y _ S I E G E } 	 	 	 	 	 	 	 	 A u t o m a t i c a l l y   d e p l o y   s i e g e   e q u i p m e n t  
 { B M T _ P A U S E D } 	 	 	 	 	 	 	 	 	 	 	 P A U S E D  
 { B M T _ W A I T I N G _ T O _ D E P L O Y } 	 	 	 	 	 	 	 	 W a i t i n g   f o r   o t h e r   p l a y e r s   t o   s t a r t   d e p l o y m e n t   p h a s e  
 { B M T _ W A I T I N G _ T O _ E N D _ D E P L O Y M E N T } 	 	 	 	 	 	 W a i t i n g   f o r   o t h e r   p l a y e r s   t o   e n d   d e p l o y m e n t   p h a s e  
 { B M T _ W A I T I N G _ T O _ E N D _ B A T T L E } 	 	 	 	 	 	 	 W a i t i n g   f o r   o t h e r   p l a y e r s   t o   e n d   b a t t l e  
 { B M T _ S E L E C T _ N E X T _ A R M Y } 	 	 	 	 	 	 	 	 C y c l e   f o r w a r d   t h r o u g h   a r m y   l i s t  
 { B M T _ S E L E C T _ P R E V _ A R M Y } 	 	 	 	 	 	 	 	 C y c l e   b a c k w a r d   t h r o u g h   a r m y   l i s t  
 { B M T _ P E R C E N T A G E _ A L L I E S _ K I L L E D } 	 	 	 	 	 	 P e r c e n t a g e   A l l i e s   K i l l e d :  
 { B M T _ P E R C E N T A G E _ E N E M I E S _ K I L L E D } 	 	 	 	 	 	 P e r c e n t a g e   E n e m i e s   K i l l e d :  
 { B M T _ E X I T _ B A T T L E } 	 	 	 	 	 	 	 	 	 	 E x i t   B a t t l e  
 { B M T _ R E T U R N _ T O _ C A M P A I G N } 	 	 	 	 	 	 	 	 R e t u r n   t o   C a m p a i g n   M a p  
 { B M T _ L I N K _ U N I T S } 	 	 	 	 	 	 	 	 	 E n a b l e   u n i t   l i n k i n g  
 { B M T _ U N L I N K _ U N I T S } 	 	 	 	 	 	 	 	 	 D i s a b l e   u n i t   l i n k i n g  
 { B M T _ E N A B L E _ A I _ G R O U P I N G } 	 	 	 	 	 	 	 	 P u t   g r o u p   u n d e r   a i   a s s i s t a n c e  
 { B M T _ D I S A B L E _ A I _ G R O U P I N G } 	 	 	 	 	 	 	 	 R e m o v e   g r o u p   f r o m   a i   a s s i s t a n c e  
 { B M T _ B A T T L E _ R E S O L U T I O N _ A U T O _ R E S O L V E _ Q U I T T E R _ L O S E S } 	 I f   y o u   q u i t   n o w   y o u   w i l l   l o s e   t h i s   b a t t l e  
 { B M T _ B A T T L E _ R E S O L U T I O N _ S U C C E S S F U L _ S A L L Y _ O U T } 	 	 	 Y o u r   s a l l y   h a s   b e e n   s u c c e s s f u l .     Y o u   m a y   l e a v e   t h e   b a t t l e  
 { B M T _ B A T T L E _ R E S O L U T I O N _ S U C C E S S F U L _ W I T H D R A W } 	 	 	 Y o u   h a v e   s u c c e s s f u l l y   w i t h d r a w n   f r o m   t h e   b a t t l e .     Y o u   m a y   l e a v e   t h e   b a t t l e  
 { B M T _ B A T T L E _ R E S O L U T I O N _ Q U I T _ P R O L O G U E } 	 	 	 	 	 Q u i t t i n g   n o w   w i l l   q u i t   t h e   w h o l e   t u t o r i a l .     A r e   y o u   s u r e   y o u   w a n t   t o   l e a v e ?  
 { B M T _ C O N D I T I O N S _ C O M P L E T E } 	 	 	 	 	 	 	 	 V i c t o r y  
 { B M T _ C O N D I T I O N S _ C O M P L E T E _ C O N T I N U E } 	 	 	 	 	 Y o u   h a v e   c o m p l e t e d   t h e   o b j e c t i v e s   f o r   t h i s   b a t t l e !     W o u l d   y o u   l i k e   t o   c o n t i n u e   a n d   h u n t   t h e   e n e m y   d o w n   l i k e   t h e   w o r t h l e s s   a n i m a l s   t h e y   a r e ?  
 { B M T _ S A V E _ R E P L A Y } 	 	 	 	 	 	 	 	 	 	 S a v e   B a t t l e   R e p l a y  
 { B M T _ F L A M I N G _ A M M O _ S P E C I A L } 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   F i r e   f l a m i n g   a m m o  
 { B M T _ W A R C R Y _ S P E C I A L } 	 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   W a r c r y  
 { B M T _ C H A N T _ S P E C I A L } 	 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   D r u i d i c   c h a n t  
 { B M T _ C U R S E _ S P E C I A L } 	 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   S c r e e c h i n g   w o m e n  
 { B M T _ B E R S E R K _ S P E C I A L } 	 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   B e r s e r k   a t t a c k  
 { B M T _ R A L L Y _ S P E C I A L } 	 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   R a l l y   t r o o p s  
 { B M T _ W E D G E _ S P E C I A L } 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   W e d g e   f o r m a t i o n  
 { B M T _ K I L L _ E L E P H A N T S _ S P E C I A L } 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   K i l l   r a m p a g i n g   e l e p h a n t s  
 { B M T _ M O V E _ A N D _ S H O O T _ S P E C I A L } 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   H o r s e   a r c h e r s   f i r e   a t   w i l l  
 { B M T _ C A N T A B R I A N _ C I R C L E _ S P E C I A L } 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   C a n t a b r i a n   c i r c l e \ n H o r s e   a r c h e r s   c i r c l e   e n e m y  
 { B M T _ S H I E L D _ W A L L _ S P E C I A L } 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   S h i e l d   w a l l  
 { B M T _ S T E A L T H _ S P E C I A L } 	 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   S t e a l t h \ n M o v e   w h i l e   h i d d e n  
 { B M T _ F E I G N E D _ R O U T _ S P E C I A L } 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   F e i g n e d   r o u t  
 { B M T _ N O _ S P E C I A L _ A B I L I T Y } 	 	 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   N o n e  
 { B M T _ A L L _ A L L I E S } 	 	 	 	 	 	 	 	 	 A l l   a l l i e s  
 { B M T _ A L L _ E N E M I E S } 	 	 	 	 	 	 	 	 	 A l l   e n e m i e s  
 { B M T _ C O N T I N U E _ B A T T L E _ A F T E R _ V I C T O R Y } 	 	 	 	 	 	 C o n t i n u e   B a t t l e 	 	 	 	 	 	  
 { B M T _ Q U I T _ B A T T L E _ A F T E R _ V I C T O R Y } 	 	 	 	 	 	 E n d   B a t t l e  
 { B M T _ G R O U P _ F O R M A T I O N _ 1 } 	 	 	 	 U n i t   G r o u p   F o r m a t i o n :   S i n g l e   L i n e ,   U n s o r t e d   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 2 } 	 	 	 	 U n i t   G r o u p   F o r m a t i o n :   S i n g l e   L i n e ,   S o r t e d   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 3 } 	 	 	 	 U n i t   G r o u p   F o r m a t i o n :   D o u b l e   L i n e ,   U n s o r t e d   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 4 } 	 	 	 	 U n i t   G r o u p   F o r m a t i o n :   D o u b l e   L i n e ,   S o r t e d   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 5 } 	 	 	 	 U n i t   G r o u p   F o r m a t i o n :   R e p u b l i c a n   R o m a n   L e g i o n   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 6 } 	 	 	 	 U n i t   G r o u p   F o r m a t i o n :   I m p e r i a l   R o m a n   L e g i o n   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 7 } 	 	 	 	 U n i t   G r o u p   F o r m a t i o n :   G r e e k / E a s t e r n / B a r b a r i a n   A r m y   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 8 } 	 	 	 	 U n i t   G r o u p   F o r m a t i o n :   C o l u m n   F o r m a t i o n  
 { B M T _ C H A T _ A L L _ P L A Y E R S } 	 	 A l l   P l a y e r s  
 { B M T _ N E T W O R K _ I N F O _ P R O B L E M S } 	 E n c o u n t e r e d   n e t w o r k   p r o b l e m s  
 { B M T _ N E T W O R K _ I N F O _ R E T R Y } 	 R e t r y  
 { B M T _ A B A N D O N _ G A M E } 	 	 A b a n d o n   G a m e  
 { B M T _ G R O U P _ F O R M A T I O N _ 1 } 	 	 S i n g l e   L i n e ,   U n s o r t e d   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 2 } 	 	 S i n g l e   L i n e ,   S o r t e d   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 3 } 	 	 D o u b l e   L i n e ,   U n s o r t e d   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 4 } 	 	 D o u b l e   L i n e ,   S o r t e d   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 5 } 	 	 R e p u b l i c a n   R o m a n   L e g i o n   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 6 } 	 	 I m p e r i a l   R o m a n   L e g i o n   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 7 } 	 	 G r e e k / E a s t e r n / B a r b a r i a n   A r m y   F o r m a t i o n  
 { B M T _ G R O U P _ F O R M A T I O N _ 8 } 	 	 C o l u m n   F o r m a t i o n  
 { B M T _ C A N T A B R I A N _ C I R C L E _ S P E C I A L } 	 	 	 	 	 	 S p e c i a l   A b i l i t y :   C a n t a b r i a n   C i r c l e \ n M i s s i l e   c a v a l r y   c i r c l e   e n e m y  
 { B M T _ U N I T _ H E A V Y _ I N F A N T R Y } 	 	 H e a v y   I n f a n t r y  
 { B M T _ U N I T _ L I G H T _ I N F A N T R Y } 	 	 L i g h t   I n f a n t r y  
 { B M T _ U N I T _ S K I R M I S H E R _ I N F A N T R Y } 	 S k i r m i s h e r s  
 { B M T _ U N I T _ S P E A R M E N } 	 	 	 S p e a r m e n  
 { B M T _ U N I T _ M I S S I L E } 	 	 	 M i s s i l e  
 { B M T _ U N I T _ H E A V Y _ C A V A L R Y } 	 	 H e a v y   C a v a l r y  
 { B M T _ U N I T _ L I G H T _ C A V A L R Y } 	 	 L i g h t   C a v a l r y  
 { B M T _ U N I T _ S K I R M I S H E R _ C A V A L R Y } 	 	 S k i r m i s h e r   C a v a l r y  
 { B M T _ U N I T _ M I S S I L E _ C A V A L R Y } 	 	 M i s s i l e   C a v a l r y  
 { B M T _ U N I T _ H E A V Y _ S I E G E } 	 	 	 H e a v y   A r t i l l e r y  
 { B M T _ U N I T _ L I G H T _ S I E G E } 	 	 	 L i g h t   A r t i l l e r y  
 { B M T _ U N I T _ S I E G E } 	 	 	 	 A r t i l l e r y  
 { B M T _ U N I T _ H E A V Y _ S P E C I A L } 	 	 H e a v y  
 { B M T _ U N I T _ L I G H T _ S P E C I A L } 	 	 L i g h t  
 { B M T _ T O G G L E _ S P E C I A L _ F O R M A T I O N _ O F F } 	 	 D i s a b l e   u n i t ' s   s p e c i a l   a b i l i t y  
 { B M T _ T I M E _ R E M A I N I N G _ H O U R S } 	 	 	 T i m e   r e m a i n i n g :   % d   h o u r s  
 { B M T _ T I M E _ R E M A I N I N G _ M I N S } 	 	 	 T i m e   r e m a i n i n g :   % d   m i n u t e s  
 { B M T _ N E W _ P L A Y } 	 	 	 	 	 N o r m a l   g a m e   s p e e d  
 { B M T _ D E S T R O Y _ C H A R A C T E R } 	 	 	 	 D e s t r o y   c h a r a c t e r :  
 { B M T _ W A I T } 	 	 	 	 	 	 W A I T :  
 { B M T _ C A N T A B R I A N _ C I R C L E _ S P E C I A L }   	 S p e c i a l   A b i l i t y :   C a n t a b r i a n   C i r c l e \ n M i s s i l e   c a v a l r y   c i r c l e   i n   f r o n t   o f   e n e m y  
 { B M T _ C O M _ W I N _ B I G } 	 	 	 V i c t o r y   s e e m s   c e r t a i n .   O n l y   a   f o o l   c o u l d   l o s e   t h i s   b a t t l e  
 { B M T _ C O M _ W I N } 	 	 	 	 V i c t o r y   i s   a l m o s t   a   c e r t a i n t y  
 { B M T _ C O M _ W I N _ S L I G H T } 	 	 	 V i c t o r y   i s   a   d i s t i n c t   p o s s i b i l i t y  
 { B M T _ C O M _ E V E N } 	 	 	 	 T h e   b a l a n c e   o f   f o r c e s   i s   e v e n l y   m a t c h e d  
 { B M T _ C O M _ L O S E _ S L I G H T } 	 	 	 D e f e a t   i s   a   d i s t i n c t   p o s s i b i l i t y  
 { B M T _ C O M _ L O S E } 	 	 	 	 D e f e a t   i s   a l m o s t   a   c e r t a i n t y  
 { B M T _ C O M _ L O S E _ B I G } 	 	 	 D e f e a t   s e e m s   c e r t a i n .   O n l y   a   m i l i t a r y   g e n i u s   c o u l d   w i n   t h i s   b a t t l e  
 { B M T _ G R O U P _ F O R M A T I O N _ 2 } 	 	 S i n g l e   L i n e ,   S o r t e d   F o r m a t i o n  
 { B M T _ S T A T E _ A I _ L I N K E D } 	 	 A i   L i n k e d  
 { B M T _ S T A T E _ R U N N I N G _ A M O K } 	 R u n n i n g   A m o k  
 { B M T _ S T A T E _ F I G H T I N G _ T O _ D E A T H } 	 F i g h t i n g   t o   t h e   d e a t h  
 { B M T _ S T A T E _ W I T H D R A W I N G } 	 	 W i t h d r a w i n g  
 { B M T _ E N D _ D E P L O Y M E N T _ S A L L Y } 	 E N D   D E P L O Y M E N T   A N D   S T A R T   B A T T L E  
 { B M T _ N E T W O R K _ P L A Y E R S _ J O I N I N G } 	 W a i t i n g   f o r   t h e   f o l l o w i n g   p l a y e r s   t o   j o i n :  
 { B M T _ A C T _ C E L E B R A T I N G } 	 C e l e b r a t i n g  
 { B M T _ N O _ T I M E _ L I M I T } 	 N o   t i m e   l i m i t   f o r   t h i s   b a t t l e  
 { B M T _ T U R T L E _ O F F } 	 	 	 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   R e m o v e   f r o m   t e s t u d o   f o r m a t i o n  
 { B M T _ P H A L A N X _ O F F } 	 	 	 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   R e m o v e   f r o m   p h a l a n x   f o r m a t i o n  
 { B M T _ F L A M I N G _ A M M O _ S P E C I A L _ O F F } 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   F i r e   n o r m a l   a m m o  
 { B M T _ W A R C R Y _ S P E C I A L _ O F F } 	 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   S t o p   w a r c r y  
 { B M T _ C H A N T _ S P E C I A L _ O F F } 	 	 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   S t o p   d r u i d i c   c h a n t  
 { B M T _ C U R S E _ S P E C I A L _ O F F } 	 	 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   S t o p   s c r e e c h i n g  
 { B M T _ B E R S E R K _ S P E C I A L _ O F F } 	 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   S t o p   b e r s e r k   a t t a c k  
 { B M T _ W E D G E _ S P E C I A L _ O F F } 	 	 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   R e m o v e   f r o m   w e d g e   f o r m a t i o n  
 { B M T _ C A N T A B R I A N _ C I R C L E _ S P E C I A L _ O F F } 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   S t o p   c a n t a b r i a n   c i r c l e  
 { B M T _ M O V E _ A N D _ S H O O T _ S P E C I A L _ O F F } 	 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   S t o p   h o r s e   a r c h e r s   f i r i n g   a t   w i l l  
 { B M T _ 1 _ H O U R _ R E M A I N I N G } 	 	 	 1   h o u r   r e m a i n i n g  
 { B M T _ 1 _ M I N _ R E M A I N I N G } 	 	 	 1   m i n u t e   r e m a i n i n g  
 { B M T _ A I _ C O N T R O L L E D } 	 	 	 A I   R e i n f o r c e m e n t s  
 { B M T _ C H O O S E _ A N O T H E R _ T A R G E T _ W A L L } 	 	 T h i s   w a l l   s e c t i o n   h a s   b e e n   b r e a c h e d  
 { B M T _ C H O O S E _ A N O T H E R _ T A R G E T _ T O W E R } 	 	 T h i s   t o w e r   h a s   b e e n   c o m p l e t e l y   d e s t r o y e d  
 { B M T _ C H O O S E _ A N O T H E R _ T A R G E T _ G A T E } 	 	 T h e   g a t e s   h a v e   b e e n   c o m p l e t e l y   d e s t r o y e d  
 { B M T _ C H O O S E _ A N O T H E R _ T A R G E T _ B U I L D I N G } 	 T h i s   b u i l d i n g   h a s   b e e n   c o m p l e t e l y   d e s t r o y e d  
 { B M T _ C H O O S E _ A N O T H E R _ T A R G E T _ G A T E H O U S E } 	 T h e   g a t e h o u s e   h a s   b e e n   c o m p l e t e l y   d e s t r o y e d  
 { B M T _ E N G I N E _ D E S T R O Y E D } 	 	 	 	 T h i s   e q u i p m e n t   h a s   b e e n   d e s t r o y e d  
 { B M T _ T H I S _ U N I T _ S E L E C T I O N _ C A N N O T _ E Q U I P } 	 T h e   s e l e c t e d   u n i t   c a n n o t   e q u i p   t h i s   i t e m  
 { B M T _ T H E S E _ U N I T S _ C A N N O T _ E Q U I P } 	 	 T h e   s e l e c t e d   u n i t s   c a n n o t   e u i p   t h i s   i t e m  
 { B M T _ T H I S _ U N I T _ C A N N O T _ A T T A C K _ W A L L S } 	 	 T h e   s e l e c t e d   u n i t   c a n n o t   a t t a c k   w a l l s  
 { B M T _ T H E S E _ U N I T S _ C A N N O T _ A T T A C K _ W A L L S } 	 T h e   s e l e c t e d   u n i t s   c a n n o t   a t t a c k   w a l l s  
 { B M T _ T H I S _ U N I T _ H A S _ L E F T _ T H E _ B A T T L E } 	 	 T h i s   u n i t   h a s   l e f t   t h e   b a t t l e  
 { B M T _ C A N N O T _ R E A C H _ T H I S _ P O S I T I O N } 	 	 T h e   s e l e c t e d   u n i t s   c a n n o t   r e a c h   t h i s   p o s i t i o n  
 { B M T _ C A N N O T _ R E A C H _ T H I S _ P O S I T I O N _ S I N G L E } 	 T h e   s e l e c t e d   u n i t   c a n n o t   r e a c h   t h i s   p o s i t i o n  
 { B M T _ R E P L A Y _ F I N I S H E D } 	 	 R e p l a y   F i n i s h e d  
 { B M T _ P A U S E } 	 	 	 	 T o g g l e   P a u s e  
 { B M T _ T H I S _ U N I T _ C A N N O T _ A T T A C K _ T O W E R S } 	 T h e   s e l e c t e d   u n i t   c a n n o t   a t t a c k   t o w e r s  
 { B M T _ T H E S E _ U N I T S _ C A N N O T _ A T T A C K _ T O W E R S } 	 T h e   s e l e c t e d   u n i t s   c a n n o t   a t t a c k   t o w e r s  
 { B M T _ T H I S _ U N I T _ C A N N O T _ A T T A C K _ G A T E S } 	 	 T h e   s e l e c t e d   u n i t   c a n n o t   a t t a c k   g a t e s  
 { B M T _ T H E S E _ U N I T S _ C A N N O T _ A T T A C K _ G A T E S } 	 T h e   s e l e c t e d   u n i t s   c a n n o t   a t t a c k   g a t e s  
 { B M T _ T H I S _ U N I T _ C A N N O T _ A T T A C K _ B U I L D I N G S } 	 T h e   s e l e c t e d   u n i t   c a n n o t   a t t a c k   b u i l d i n g s  
 { B M T _ T H E S E _ U N I T S _ C A N N O T _ A T T A C K _ B U I L D I N G S } 	 T h e   s e l e c t e d   u n i t s   c a n n o t   a t t a c k   b u i l d i n g s  
 { B M T _ C H A T _ R E T R E A T _ G E N E R A L } 	 	 	 W i t h d r a w   a l l   t r o o p s   n o w !  
 { B M T _ F I G H T _ A G A I N } 	 	 	 	 	 F i g h t   t h i s   b a t t l e   a g a i n ?  
 { B M T _ E N A B L E _ A I _ G R O U P I N G } 	 	 	 	 	 P u t   g r o u p   u n d e r   A I   a s s i s t a n c e  
 { B M T _ C A N N O T _ M O V E _ M U L T I P L E _ U N I T S _ O N _ W A L L S } 	 	 	 C a n n o t   m o v e   m u l t i p l e   u n i t s   o n   w a l l s  
 { B M T _ R E T U R N _ O U T S I D E _ P L A Y A B L E _ A R E A } 	 	 T h i s   p o s i t i o n   i s   o u t s i d e   t h e   e x t e n t s   o f   t h e   b a t t l e f i e l d  
 { B M T _ R E T U R N _ O U T S I D E _ D E P L O Y M E N T _ A R E A } 	 C a n n o t   d e p l o y   o u t s i d e   t h e   m a r k e d   a r e a  
 { B M T _ R E T U R N _ S I E G E _ E N G I N E S _ I N _ F O R E S T } 	 C a n n o t   m o v e   s i e g e   e n g i n e s   i n s i d e   w o o d e d   a r e a s  
 { B M T _ R E T U R N _ S I E G E _ E N G I N E S _ I N _ S E T T L E M E N T S } 	 C a n n o t   m o v e   s i e g e   e n g i n e s   w h e n   t h e y   a r e   i n s i d e   s e t t l e m e n t s  
 { B M T _ R E T U R N _ U N I T S _ O N _ S I E G E _ T O W E R } 	 	 C a n n o t   m o v e   s e l e c t e d   u n i t s   o n t o   s i e g e   t o w e r s  
 { B M T _ R E T U R N _ U N I T _ O N _ S I E G E _ T O W E R } 	 	 C a n n o t   m o v e   s e l e c t e d   u n i t   o n t o   s i e g e   t o w e r s  
 { B M T _ R E T U R N _ O B S T R U C T I O N } 	 	 	 C a n n o t   m o v e   h e r e   a s   t h e   p a t h   i s   o b s t r u c t e d  
 { B M T _ T H I S _ U N I T _ I S _ Y E T _ T O _ J O I N _ B A T T L E } 	 T h i s   u n i t   i s   y e t   t o   e n t e r   t h e   b a t t l e f i e l d  
 { B M T _ P L A Z A _ P R E V I O U S _ B A N N E R _ T O O L T I P } 	 	 H o l d   t h e   p l a z a   u n t i l   t h e   c o u n t e r   r e a c h e s   z e r o   t o   c a p t u r e   t h i s   s e t t l e m e n t  
 { B M T _ P L A Z A _ C U R R E N T _ B A N N E R _ T O O L T I P } 	 	 H o l d   t h e   p l a z a   u n t i l   t h e   c o u n t e r   r e a c h e s   z e r o   t o   c a p t u r e   t h i s   s e t t l e m e n t  
 { B M T _ Z O O M _ T O _ U N I T } 	 	 	 	 L o c a t e   t h i s   u n i t  
 { B M T _ W I T H _ M I N _ 1 _ A L L Y } 	 	 	 	 W i t h   a t   l e a s t   1   a l l i e d   s o l d i e r  
 { B M T _ W I T H _ M A X _ 1 _ E N E M Y } 	 	 	 	 A n d   a t   m o s t   1   e n e m y   s o l d i e r  
 { B M T _ W I T H _ M A X _ 0 _ E N E M Y } 	 	 	 	 A n d   a t   m o s t   n o   e n e m y   s o l d i e r s  
 { B M T _ W A R C R Y _ S P E C I A L _ O F F } 	 	 	 S p e c i a l   A b i l i t y :   W a r c r y   A c t i v e  
 { B M T _ R A L L Y _ S P E C I A L _ O F F } 	 	 	 	 S p e c i a l   A b i l i t y :   A t t e m p t i n g   t o   r a l l y   t r o o p s  
 { B M T _ G R O U P _ F O R M A T I O N _ 2 } 	 	 S i n g l e   L i n e ,   S o r t e d   F o r m a t i o n  
 { B M T _ C A N N O T _ M O V E _ M U L T I P L E _ U N I T S _ O N _ B R I D G E S } 	 C a n n o t   m o v e   m u l t i p l e   u n i t s   o n t o   b r i d g e s  
 { B M T _ C A N N O T _ M O V E _ O N T O _ S I E G E _ T O W E R S } 	 	 	 C a n n o t   m o v e   d i r e c t l y   o n t o   s i e g e   t o w e r s  
 { B M T _ C A N N O T _ M O V E _ O N _ W A T E R } 	 	 	 	 C a n n o t   m o v e   o n t o   w a t e r  
 { B M T _ F I E L D _ C U R R E N T _ B A N N E R _ T O O L T I P } 	 	 	 T h e   d e f e n d e r   w i l l   w i n   t h i s   b a t t l e   w h e n   t h e   t i m e r   r e a c h e s   z e r o  
 { B M T _ N E T W O R K _ S T A L L _ D I A L O G _ T I T L E } 	 	 	 P o s s i b l e   C o n n e c t i o n   P r o b l e m s  
 { B M T _ N E T W O R K _ D R O P _ D R O P _ P L A Y E R } 	 	 	 K i c k   t h i s   p l a y e r ?  
 { B M T _ Q U I T _ F R O M _ N E T W O R K _ D R O P } 	 	 	 	 Q u i t   g a m e ?  
 { B M T _ T H I S _ U N I T _ S E L E C T I O N _ C A N N O T _ E Q U I P } 	 T h e   s e l e c t e d   u n i t   c a n n o t   b e   e q u i p p e d   w i t h   t h i s   i t e m  
 { B M T _ T H E S E _ U N I T S _ C A N N O T _ E Q U I P } 	 	 T h e   s e l e c t e d   u n i t s   c a n n o t   b e   e q u i p p e d   w i t h   t h i s   i t e m  
 { B M T _ S C H I L T R O M } 	 	 S p e c i a l   A b i l i t y :   S c h i l t r o m   F o r m a t i o n  
 { B M T _ S C H I L T R O M _ O F F } 	 	 D i s a b l e   S p e c i a l   A b i l i t y :   R e m o v e   f r o m   s c h i l t r o m   f o r m a t i o n  
 { B M T _ S H I E L D _ W A L L _ S P E C I A L _ O F F } 	 D i s a b l e   S p e c i a l   A b i l i t y :   R e m o v e   f r o m   s h i e l d   w a l l   f o r m a t i o n  
 { B M T _ A C T _ B E R S E R K } 	 	 B e r s e r k  
 { B M T _ M O R _ B E S E R K } 	 	 H e r o i c  
 { B M T _ S T A T E _ B E R S E R K } 	 	 G o i n g   b e r s e r k  
 { B M T _ C H A N T _ S P E C I A L } 	 	 S p e c i a l   A b i l i t y :   C h a n t  
 { B M T _ C H A N T _ S P E C I A L _ O F F } 	 D i s a b l e   S p e c i a l   A b i l i t y :   S t o p   c h a n t  
 { B M T _ S T A T E _ S W I M M I N G } 	 	 S w i m m i n g  
 { B M T _ D I S A B L E _ A I _ G R O U P I N G } 	 R e m o v e   g r o u p   f r o m   A I   a s s i s t a n c e  
 { B M T _ C O N C E R N E D _ G E N E R A L _ J U S T _ F L E D } 	 C o n c e r n e d   b e c a u s e   t h e   g e n e r a l   h a s   f l e d   t h e   b a t t l e f i e l d  
  
 